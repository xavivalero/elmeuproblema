import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 * @version 3-2021.
 * @author Xavi Valero Calderon.
 * @since 11-3-2021.
 */

public class Problema {
	
	static Scanner reader = new Scanner(System.in);
	
	/**
	 * Main.
	 * Aqu� �s on es desenvolupa tota l'estructura del programa, i on es criden m�todes per introduir certes dades.
	 * @param args M�tode String.
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		boolean marcatrobada = false;
		int dorsaljugador = 0;
		ArrayList<Integer> llistaJugadors = new ArrayList<Integer>();
		int gols = 0;
		ArrayList<Integer> golsjugadors = new ArrayList<Integer>();
		
		
		int equips = IntroduirEquips();
		
		for(int i = 0;i < equips;i++) {
			int jugadors = IntroduirJugadors();
			
			for(int j = 0;j < jugadors;j++) {
				dorsaljugador = IntroduirDorsal();
				llistaJugadors.add(dorsaljugador);
				golsjugadors.add(0);
			}
			
			
			
			while(!marcatrobada) {
				dorsaljugador = reader.nextInt();
				
				int index = llistaJugadors.indexOf(dorsaljugador);
				
				gols = reader.nextInt();
				
				if(dorsaljugador == 0 && gols == 0) {
					marcatrobada = true;
				} else {
					golsjugadors.add(index, gols);
				}
			}
			
			Comparator<Integer> comparador = Collections.reverseOrder();
			Collections.sort(golsjugadors, comparador);
			
			for(int k = 0;k < 4;k++) {
				System.out.print(golsjugadors.get(k) + " ");
			}
			
			System.out.println();
			llistaJugadors.clear();
			golsjugadors.clear();
			marcatrobada = false;
		}
	}
	
	/**
	 * Demanar equips.
	 * Demana el n�mero d'equips amb els que volem trobar el 4 ideal.
	 * @return equips N�mero d'equips.
	 */
	
	public static int IntroduirEquips() {
		
		int equips = reader.nextInt();
		
		return equips;
	}
	
	/**
	 * Demanar jugadors.
	 * Demana el n�mero de jugadors del equip.
	 * @return jugadors N�mero de jugadors.
	 */
	
	public static int IntroduirJugadors() {
		
		int jugadors = reader.nextInt();
		
		return jugadors;
	}
	
	/**
	 * Demanar dorsals.
	 * Demana el n�mero del dorsal de cada jugador.
	 * @return dorsal N�mero de dorsal.
	 */
	
	public static int IntroduirDorsal() {
		
		int dorsal = reader.nextInt();
		
		return dorsal;
	}

}
